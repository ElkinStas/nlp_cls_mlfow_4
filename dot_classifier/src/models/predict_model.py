import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import pandas as pd
import numpy as np
import click
import mlflow
import mlflow.pytorch


def predict(model_path, test_data_path):
    """Predict using the trained model."""
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    model = AutoModelForSequenceClassification.from_pretrained(model_path).to(
        device
    )

    test_data = pd.read_csv(test_data_path)
    inputs = tokenizer(
        test_data["comment"].tolist(),
        return_tensors="pt",
        padding=True,
        truncation=True,
        max_length=512,
    ).to(device)

    with torch.no_grad():
        outputs = model(**inputs)
    predictions = (
        torch.nn.functional.softmax(outputs.logits, dim=-1)[:, 1].cpu().numpy()
    )

    pred_labels = np.where(predictions > 0.5, 1, 0)
    test_data["predictions"] = pred_labels
    test_data.to_csv("data/processed/test_predictions.csv", index=False)

    mlflow.log_artifact("data/processed/test_predictions.csv")
    return test_data


@click.command()
@click.option(
    "--model_path",
    default="./trained_model",
    help="Path to the trained model",
)
@click.option(
    "--test_data_path",
    default="data/processed/test.csv",
    help="Path to the test data",
)
def cli(model_path, test_data_path):
    """CLI for making predictions using the trained model."""
    mlflow.set_tracking_uri("http://127.0.0.1:5000")
    mlflow.set_experiment("Model Prediction Experiment")

    with mlflow.start_run():
        predict(model_path, test_data_path)


if __name__ == "__main__":
    cli()
