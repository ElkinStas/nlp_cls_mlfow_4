import pandas as pd
import torch
from transformers import (
    AutoModelForSequenceClassification,
    Trainer,
    TrainingArguments,
    AutoTokenizer,
)
from datasets import Dataset, DatasetDict
import click
import mlflow
import mlflow.pytorch

# Создаем токенайзер
tokenizer = AutoTokenizer.from_pretrained("cointegrated/rubert-tiny2")


def tokenize_function(examples):
    """Tokenize the dataset."""
    tokenized_inputs = tokenizer(
        examples["comment"], padding="max_length", truncation=True
    )
    tokenized_inputs["labels"] = [int(label) for label in examples["toxic"]]
    return tokenized_inputs


def train_model(
    train_dataset, test_dataset, model_name="cointegrated/rubert-tiny2"
):
    """Train the model on the given dataset."""
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = AutoModelForSequenceClassification.from_pretrained(
        model_name, num_labels=2
    ).to(device)

    training_args = TrainingArguments(
        output_dir="./results",
        eval_strategy="epoch",
        learning_rate=2e-5,
        per_device_train_batch_size=5,
        per_device_eval_batch_size=5,
        num_train_epochs=4,
        weight_decay=0.01,
        logging_dir="./logs",
        logging_steps=10,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=test_dataset,
        tokenizer=tokenizer,
    )

    trainer.train()
    model.save_pretrained("./trained_model")
    return model


@click.command()
@click.option(
    "--train_path",
    default="data/processed/train.csv",
    help="Path to the training dataset",
)
@click.option(
    "--test_path",
    default="data/processed/test.csv",
    help="Path to the testing dataset",
)
def cli(train_path, test_path):
    """CLI for training the model."""
    train_data = pd.read_csv(train_path, delimiter=",")
    test_data = pd.read_csv(test_path, delimiter=",")

    train_dataset = Dataset.from_pandas(train_data)
    test_dataset = Dataset.from_pandas(test_data)

    tokenized_datasets = DatasetDict(
        {"train": train_dataset, "test": test_dataset}
    ).map(tokenize_function, batched=True)

    mlflow.set_tracking_uri("http://127.0.0.1:5000")
    mlflow.set_experiment("Model Training Experiment")

    with mlflow.start_run():
        mlflow.log_param("model_name", "cointegrated/rubert-tiny2")
        mlflow.log_param("learning_rate", 2e-5)
        mlflow.log_param("per_device_train_batch_size", 5)
        mlflow.log_param("per_device_eval_batch_size", 5)
        mlflow.log_param("num_train_epochs", 4)
        mlflow.log_param("weight_decay", 0.01)

        model = train_model(
            tokenized_datasets["train"], tokenized_datasets["test"]
        )

        mlflow.pytorch.log_model(model, "model")


if __name__ == "__main__":
    cli()
