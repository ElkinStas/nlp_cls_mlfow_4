# tests/test_environment.py

import sys


def test_environment():
    """Test that environment is set up correctly."""
    assert sys.version_info.major == 3
    assert sys.version_info.minor >= 6


if __name__ == "__main__":
    test_environment()
    print("Environment is set up correctly.")
