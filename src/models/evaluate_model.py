import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from datasets import load_dataset
import numpy as np
from sklearn.metrics import accuracy_score, f1_score
import click
import mlflow
import mlflow.pytorch


def evaluate(
    model_path="./trained_model", test_data_path="data/processed/test.csv"
):
    """Evaluate the model on the test dataset."""
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    model = AutoModelForSequenceClassification.from_pretrained(model_path).to(
        device
    )

    dataset = load_dataset("csv", data_files=test_data_path)["train"]
    texts = dataset["comment"]
    labels = dataset["toxic"]

    inputs = tokenizer(
        texts,
        return_tensors="pt",
        padding=True,
        truncation=True,
        max_length=512,
    ).to(device)

    with torch.no_grad():
        outputs = model(**inputs)
    predictions = (
        torch.nn.functional.softmax(outputs.logits, dim=-1)[:, 1].cpu().numpy()
    )

    pred_labels = np.where(predictions > 0.5, 1, 0)

    accuracy = accuracy_score(labels, pred_labels)
    f1 = f1_score(labels, pred_labels)

    print(f"Accuracy: {accuracy}")
    print(f"F1 Score: {f1}")

    # Логирование метрик в MLflow
    mlflow.log_metric("accuracy", accuracy)
    mlflow.log_metric("f1_score", f1)


@click.command()
@click.option(
    "--model_path",
    type=str,
    default="./trained_model",
    help="Path to the trained model",
)
@click.option(
    "--test_data_path",
    type=str,
    default="data/processed/test.csv",
    help="Path to the test data",
)
def cli(model_path, test_data_path):
    """CLI for evaluating the model."""
    mlflow.set_tracking_uri("http://127.0.0.1:5000")
    mlflow.set_experiment("Model Evaluation Experiment")

    with mlflow.start_run():
        evaluate(model_path, test_data_path)


if __name__ == "__main__":
    cli()
