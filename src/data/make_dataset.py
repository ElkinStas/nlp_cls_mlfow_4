import pandas as pd
from sklearn.model_selection import train_test_split
import mlflow
import mlflow.sklearn


def main():
    """
    Создание обучающих и тестовых наборов данных из исходного файла,
    а также логирование артефактов в MLflow.
    """
    # Установите URI отслеживания для MLflow
    mlflow.set_tracking_uri("http://127.0.0.1:5000")
    mlflow.set_experiment("Dataset Creation Experiment")

    # Начало эксперимента в MLflow
    print("Starting MLflow run...")
    mlflow.start_run()

    try:
        data = pd.read_csv("data/raw/labeled.csv")
        print("Data loaded successfully.")

        # Удаление лишних переносов строк
        data["comment"] = (
            data["comment"].str.replace("\n", " ").str.replace("\r", "")
        )

        train, test = train_test_split(data, test_size=0.2, random_state=42)
        train.to_csv("data/processed/train.csv", index=False)
        test.to_csv("data/processed/test.csv", index=False)
        print("Train and test datasets created and saved.")

        # Логирование артефактов в MLflow
        mlflow.log_artifact("data/processed/train.csv")
        mlflow.log_artifact("data/processed/test.csv")
        print("Artifacts logged to MLflow.")

    except Exception as error:
        print(f"An error occurred: {error}")

    finally:
        # Завершение эксперимента в MLflow
        mlflow.end_run()
        print("MLflow run ended.")


if __name__ == "__main__":
    main()
