from setuptools import find_packages, setup

setup(
    name="dot_classifier",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    version="0.1.0",
    description="short project for ml ops course in itmo",
    author="Elkin Stas",
    license="MIT",
    install_requires=[
        "click",
        "Sphinx",
        "coverage",
        "awscli",
        "flake8",
        "python-dotenv>=0.5.1",
        "transformers",
        "datasets",
        "torch",
        "scikit-learn",
    ],
    tests_require=["pytest",],
    entry_points={
        "console_scripts": ["check_toxicity=src.check_toxicity:main",],
    },
)
