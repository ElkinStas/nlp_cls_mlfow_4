FROM python:3.8-slim

RUN pip install mlflow boto3 psycopg2-binary

ENV BACKEND_STORE_URI=""
ENV ARTIFACT_STORE_URI=""

CMD ["sh", "-c", "mlflow server --backend-store-uri $BACKEND_STORE_URI --default-artifact-root $ARTIFACT_STORE_URI --host 0.0.0.0"]

